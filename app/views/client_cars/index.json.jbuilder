json.array!(@client_cars) do |client_car|
  json.extract! client_car, :id, :client_id, :brand, :model, :license_plate, :vin
  json.url client_car_url(client_car, format: :json)
end
