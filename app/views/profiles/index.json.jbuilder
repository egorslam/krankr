json.array!(@profiles) do |profile|
  json.extract! profile, :id, :worker_id, :first_name, :last_name, :third_name, :date_of_birthday, :photo_url, :comment, :rank
  json.url profile_url(profile, format: :json)
end
