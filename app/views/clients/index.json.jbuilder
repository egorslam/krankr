json.array!(@clients) do |client|
  json.extract! client, :id, :name, :phone, :vin_code, :license_plate, :car_model
  json.url client_url(client, format: :json)
end
