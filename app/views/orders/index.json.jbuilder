json.array!(@orders) do |order|
  json.extract! order, :id, :worker_id, :client_id, :service_type_id, :detail_info, :status, :total_amount
  json.url order_url(order, format: :json)
end
