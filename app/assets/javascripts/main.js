$(document).ready(function() {

  $('#datetimepicker2').datepicker({
    language: "ru",
    todayHighlight: true,
    orientation: "bottom left",
    format: 'yyyy/mm/dd',
    autoclose: true
  });

  $('#datetimepicker1').datetimepicker({
    format : 'YYYY/MM/DD HH:mm'
  });

  $('#datetimepicker2').change(function(){
    date = $('#datetimepicker2 input').val();
    window.location.replace('/admin/orders_by_status/' + date +'/');
    //  $.ajax({
    //   type: 'GET',
    //   url: '/orders_by_status/' + date
    // });
  });

  tinymce.remove();
  tinyMCE.init({
    selector: "textarea.tinymce",
    plugins: "image, link, code",
    toolbar: "styleselect | undo redo | removeformat | bold italic underline |  aligncenter alignjustify  | bullist numlist outdent indent | link | code"
  });

  CalcTotalAmount();
  $('#order_service_type_ids').click(function(){
    CalcTotalAmount();
  })
  $('option').mousedown(function(e) {
    e.preventDefault();
    $(this).prop('selected', !$(this).prop('selected'));
    return false;
  });
  cbpAnimatedHeader()
});

function showClientCarForm(btn){
  $(btn).hide(300)
  $('#new_client_car').show(300)
}

function hideClientCarForm(){
  $('#show_form_button').show(300)
  $('#new_client_car').hide(300)
}

function getClientCars(client){
  $.ajax({
    type: 'GET',
    url: '/admin/get_client_car/' + $(client).val()
  }).done(function(msg){
    $('#order_client_car_id').empty();
    $.each(msg.cars, function(i, val){
      $("#order_client_car_id").append($('<option></option>').val(val.id).html(val.brand + " " + val.model));
    });
  });
}

function secondsTimeSpanToHMS(s) {
    var h = Math.floor(s/3600); //Get whole hours
    s -= h*3600;
    var m = Math.floor(s/60); //Get remaining minutes
    return h+":"+(m < 10 ? '0'+m : m); //zero padding on minutes and seconds
}

function CalcTotalAmount() {
  var amount_array = [];
  var time_array = [];
    $('#order_service_type_ids :selected').each(function(i, selected){
      amount_array[i] = [$(selected).text(), $(selected).data('amount')]
      time_array[i] = [$(selected).text(), $(selected).data('time')]
    });
    $('#total_amount').empty();
    $('#order_total_amount').empty();
    $.each(amount_array,function() {
      $('#total_amount').append('<p>' + this[0] + ': '+ this[1] + 'грн</p>')
    });
    var total_amount = 0;
    $.each(amount_array,function() {
      total_amount += this[1];
    });
    var total_time = 0;
    $.each(time_array,function() {
      total_time += this[1];
    });
    $('#total_amount').append('<h5>Cумма к оплате: ' + total_amount + 'грн</h5>')
    $('#total_amount').append('<h5>Время на выполнение: ' + secondsTimeSpanToHMS(total_time) + '</h5>')
    $('#order_total_amount').val(total_amount)
    $('#order_time_to_complete').val(total_time)
}

function cbpAnimatedHeader() {
  var docElem = document.documentElement,
    header = document.querySelector( '.navbar-default' ),
    didScroll = false,
    changeHeaderOn = 300;

  function init() {
    window.addEventListener( 'scroll', function( event ) {
      if( !didScroll ) {
        didScroll = true;
        setTimeout( scrollPage, 250 );
      }
    }, false );
  }

  function scrollPage() {
    var sy = scrollY();
    if ( sy >= changeHeaderOn ) {
      classie.add( header, 'affix' );
    }
    else {
      classie.remove( header, 'affix' );
    }
    didScroll = false;
  }

  function scrollY() {
    return window.pageYOffset || docElem.scrollTop;
  }

  init();
};