class ClientCar < ActiveRecord::Base
  belongs_to :client
  validates :model, presence: true
  validates :license_plate, presence: true

  has_many :orders, dependent: :destroy
end
