class Profile < ActiveRecord::Base
  belongs_to :worker
  validates_associated :worker

  has_many :comments

  mount_uploader :photo_url, AvatarUploader

  has_many :orders

  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :third_name, presence: true
  validates :date_of_birthday, presence: true
  validates :photo_url, presence: true
  validates :description, presence: true

  # validates_attachment :photo, content_type: { content_type: /\Aimage\/.*\Z/ },
  #                               size: { in: 0..1.megabytes }
end
