class Comment < ActiveRecord::Base
  belongs_to :profile

  validates :author_name, presence: true
  validates :body, presence: true
end
