class ServiceType < ActiveRecord::Base
  has_many :order_service_types
  has_many :orders, through: :order_service_types
  # has_many :client_bid_service_types
  # has_many :client_bid, through: :client_bid_service_types
  belongs_to :category

  validates :title, presence: true
  validates :category, presence: true
  validates :amount, presence: true
  validates :time_to_complete, presence: true
end
