class Category < ActiveRecord::Base
  has_many :service_types
  validates :title, presence: true
end
