class Order < ActiveRecord::Base
  belongs_to :client
  belongs_to :client_car
  belongs_to :profile
  has_many :order_service_types
  has_many :service_types, through: :order_service_types
  accepts_nested_attributes_for :order_service_types, allow_destroy: true
  enum status: [:in_progress, :finished, :need_check]

  default_scope {order('start_time')}

  # validates :name, presence: true

  validate :check_blank_time
  before_validation :set_end_time

  def set_end_time
    unless self.status == 'need_check'
      self.end_time = self.start_time + self.time_to_complete.seconds
    end
  end

  private

    def check_blank_time
      unless self.status == 'need_check'
        Order.all.each do |order|
          if order.start_time && order.end_time
            if (order.start_time..order.end_time).cover?(self.start_time) && order.profile_id == self.profile_id
              errors.add('!', "Работник занят с #{order.start_time.strftime("%d-%m-%y %H:%M")} по #{order.end_time.strftime("%d-%m-%y %H:%M")}")
            end
          end
        end
      end
    end
end
