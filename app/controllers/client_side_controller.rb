class ClientSideController < ApplicationController
  def index
    @news = News.where(is_action: false).limit(5)
    @actions = News.where(is_action: true).limit(5)
    @articles = Article.all.limit(5)
    render layout: 'application_client'
  end
  def actions
    @actions = News.where(is_action: true)
    render layout: 'application_client'
  end

  def contact
    render layout: 'application_client'
  end

  def articles
    @articles = Article.all
    render layout: 'application_client'
  end

  def news
    @news = News.where(is_action: false)
    render layout: 'application_client'
  end

  def service_types
    @data = {}
    ServiceType.all.each do |service_type|
      @data[service_type.category.title] ||= []
      @data[service_type.category.title] << service_type
    end
    @order = Order.new
    render layout: 'application_client'
  end

  def content_show
    if params[:content_type] == 'article'
      @object = Article.find(params[:content_id])
    elsif params[:content_type] == 'news' || params[:content_type] == 'action'
      @object = News.find(params[:content_id])
    end
    render layout: 'application_client'
  end

  def order_show
    @order = Order.find(params[:order_id])
    render layout: 'application_client'
  end

  def profiles
    @profiles = Profile.all
    render layout: 'application_client'
  end

  def profiles_show
    @profile = Profile.find(params[:profile_id])
    @comment = Comment.new
    render layout: 'application_client'
  end

  def create_order
    @order = Order.new(order_params)

    respond_to do |format|
      if @order.save
        format.html { redirect_to order_show_path(@order.id), notice: 'Ваш заказ принят. Ожидайте звонка от менеджера :)' }
        format.json { render :show, status: :created, location: @order }
      else
        flash[:error] = @comment.errors.full_messages.to_sentence
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

  def create_comment
    @comment = Comment.new(comment_params)
    respond_to do |format|
      if @comment.save
        format.html { redirect_to profiles_show_client_path(@comment.profile.id), notice: 'Спасибо. Комментарий успешно создан' }
      else
        flash[:error] = @comment.errors.full_messages.to_sentence
        format.html { redirect_to profiles_show_client_path(@comment.profile.id) }
      end
    end
  end

  def comment_params
    params.require(:comment).permit(:profile_id, :author_name, :body)
  end
  def order_params
    params.require(:order).permit(:time_to_complete,:client_id, :detail_info, :status, :phone, :name, :email, :start_time, :total_amount, service_type_ids: [])
  end
end
