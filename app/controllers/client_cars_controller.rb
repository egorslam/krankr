class ClientCarsController < ApplicationController
  before_action :set_client_car, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_worker!

  # GET /client_cars
  # GET /client_cars.json
  def index
    @client_cars = ClientCar.all
  end

  # GET /client_cars/1
  # GET /client_cars/1.json
  def show
  end

  # GET /client_cars/new
  def new
    @client_car = ClientCar.new
  end

  # GET /client_cars/1/edit
  def edit
  end

  # POST /client_cars
  # POST /client_cars.json
  def create
    @client_car = ClientCar.new(client_car_params)

    respond_to do |format|
      if @client_car.save
        format.html { redirect_to @client_car.client, notice: 'Автомобиль успешно создан' }
        format.json { render :show, status: :created, location: @client_car }
      else
        flash[:error] = @client_car.errors.full_messages.to_sentence
        format.html { redirect_to @client_car.client }
      end
    end
  end

  # PATCH/PUT /client_cars/1
  # PATCH/PUT /client_cars/1.json
  def update
    respond_to do |format|
      if @client_car.update(client_car_params)
        format.html { redirect_to @client_car.client, notice: 'Автомобиль успешно обновлен' }
        format.json { render :show, status: :ok, location: @client_car }
      else
        format.html { render :edit }
        format.json { render json: @client_car.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /client_cars/1
  # DELETE /client_cars/1.json
  def destroy
    @client_car.destroy
    respond_to do |format|
      format.html { redirect_to @client_car.client, notice: 'Client car was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_client_car
      @client_car = ClientCar.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def client_car_params
      params.require(:client_car).permit(:client_id, :brand, :model, :license_plate, :vin)
    end
end
