class Rename < ActiveRecord::Migration
  def change
    rename_column :orders, :worker_id, :profile_id
  end
end
