class DeleteRole < ActiveRecord::Migration
  def change
    drop_table :roles
    drop_table :workers_roles
  end
end
