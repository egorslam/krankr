class AddCarIdToOrder < ActiveRecord::Migration
  def change
    add_column :orders, :client_car_id, :integer
  end
end
