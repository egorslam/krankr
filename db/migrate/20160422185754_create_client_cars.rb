class CreateClientCars < ActiveRecord::Migration
  def change
    create_table :client_cars do |t|
      t.integer :client_id
      t.string :brand
      t.string :model
      t.string :license_plate
      t.string :vin

      t.timestamps null: false
    end
    remove_column :clients, :vin_code
    remove_column :clients, :license_plate
    remove_column :clients, :car_model
  end
end
