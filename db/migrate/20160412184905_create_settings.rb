class CreateSettings < ActiveRecord::Migration
  def change
    create_table :settings do |t|
      t.string :title
      t.boolean :enable, default: false
      t.timestamps null: false
    end
  end
end
