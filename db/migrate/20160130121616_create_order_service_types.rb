class CreateOrderServiceTypes < ActiveRecord::Migration
  def change
    create_table :order_service_types do |t|
      t.integer :order_id
      t.integer :service_type_id

      t.timestamps null: false
    end
  end
end
