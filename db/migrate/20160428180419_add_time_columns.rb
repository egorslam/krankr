class AddTimeColumns < ActiveRecord::Migration
  def change
    add_column :service_types, :time_to_complete, :integer
    add_column :orders, :time_to_complete, :integer
    add_column :orders, :start_time, :datetime
    add_column :orders, :end_time, :datetime
  end
end
