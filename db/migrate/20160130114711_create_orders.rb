class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.integer :worker_id
      t.integer :client_id
      t.integer :service_type_id
      t.integer :detail_info
      t.integer :status
      t.integer :total_amount, default: 0

      t.timestamps null: false
    end
  end
end
