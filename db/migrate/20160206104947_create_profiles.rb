class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.integer :worker_id
      t.string :first_name
      t.string :last_name
      t.string :third_name
      t.date :date_of_birthday
      t.string :photo_url
      t.text :comment
      t.integer :rank

      t.timestamps null: false
    end
  end
end
