class AddColumnToServiceType < ActiveRecord::Migration
  def change
    add_column :service_types, :description, :string, limit: 1024
  end
end
