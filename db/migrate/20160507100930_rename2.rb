class Rename2 < ActiveRecord::Migration
  def change
    rename_column :comments, :worker_id, :profile_id
  end
end
