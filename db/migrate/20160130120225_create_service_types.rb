class CreateServiceTypes < ActiveRecord::Migration
  def change
    create_table :service_types do |t|
      t.string :title
      t.integer :amount

      t.timestamps null: false
    end
  end
end
