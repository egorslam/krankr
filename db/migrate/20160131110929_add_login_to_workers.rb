class AddLoginToWorkers < ActiveRecord::Migration
  def change
    add_column :workers, :username, :string
    add_index :workers, :username, unique: true
  end
end
