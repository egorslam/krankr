class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.string :title

      t.timestamps null: false
    end
    add_column :service_types, :category_id, :integer
    add_index :service_types, :category_id
  end
end
