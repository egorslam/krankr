class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.integer :worker_id
      t.string :author_name
      t.text :body

      t.timestamps null: false
    end
  end
end
