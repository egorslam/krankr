class AddPhoneNameEmailToOrder < ActiveRecord::Migration
  def change
    add_column :orders, :name, :string
    add_column :orders, :phone, :string
    add_column :orders, :email, :string
    change_column :orders, :detail_info, :string
  end
end
