class CreateClients < ActiveRecord::Migration
  def change
    create_table :clients do |t|
      t.string :name
      t.string :phone
      t.string :vin_code
      t.string :license_plate
      t.string :car_model

      t.timestamps null: false
    end
  end
end
