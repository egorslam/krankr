Rails.application.routes.draw do

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  if Rails.env.production?
    client_host = 'krankr.herokuapp.com'
    host = 'localhost'
  else
    client_host = 'localhost.euro'
    host = 'localhost'
  end


  root to: "admin/dashboard#index"

  scope '/:site_id' do

    get '/', to: 'client_side#index'

    get '/contact', to: 'client_side#contact', as: 'contact'
    get '/content/show/:content_id/:content_type', to: 'client_side#content_show', as: 'show_content'
    get '/service_types', to: 'client_side#service_types', as:'service_types_client'
    get '/actions', to: 'client_side#actions', as:'actions_client'
    get '/news', to: 'client_side#news', as:'news_client'
    get '/articles', to: 'client_side#articles', as:'articles_client'
    post '/orders', to: 'client_side#create_order'
    get '/orders/:order_id', to: 'client_side#order_show', as: 'order_show'
    get '/profiles/', to: 'client_side#profiles', as: 'profiles_client'
    get '/profiles/:profile_id', to: 'client_side#profiles_show', as: 'profiles_show_client'
    get '/comment/new', to: 'client_side#profiles_show', as: "comments"
    post '/comment/new', to: 'client_side#create_comment'
  # end


    scope '/admin' do
      root 'home#index', as: 'root_server'
      resources :client_cars
      resources :profiles
      resources :articles
      resources :news
      resources :service_types
      resources :orders
      get '/orders_by_status/(:year)/(:month)/(:day)', to: 'orders#index', as: 'orders_by_status'
      resources :clients
      # resources :settings
      get '/get_client_car/:client_id', to: 'clients#get_client_car'
      get '/settings', to: 'settings#index'
      get '/settings/:id/edit', to: 'settings#edit', as: 'edit_setting'
      patch  '/settings/:id(.:format)', to: 'settings#update', as: 'setting'
      resources :categories
      devise_for :workers
    end
  end
end
