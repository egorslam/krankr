require 'test_helper'

class ClientCarsControllerTest < ActionController::TestCase
  setup do
    @client_car = client_cars(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:client_cars)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create client_car" do
    assert_difference('ClientCar.count') do
      post :create, client_car: { brand: @client_car.brand, client_id: @client_car.client_id, license_plate: @client_car.license_plate, model: @client_car.model, vin: @client_car.vin }
    end

    assert_redirected_to client_car_path(assigns(:client_car))
  end

  test "should show client_car" do
    get :show, id: @client_car
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @client_car
    assert_response :success
  end

  test "should update client_car" do
    patch :update, id: @client_car, client_car: { brand: @client_car.brand, client_id: @client_car.client_id, license_plate: @client_car.license_plate, model: @client_car.model, vin: @client_car.vin }
    assert_redirected_to client_car_path(assigns(:client_car))
  end

  test "should destroy client_car" do
    assert_difference('ClientCar.count', -1) do
      delete :destroy, id: @client_car
    end

    assert_redirected_to client_cars_path
  end
end
