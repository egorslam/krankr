require 'test_helper'

class ProfilesControllerTest < ActionController::TestCase
  setup do
    @profile = profiles(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:profiles)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create profile" do
    assert_difference('Profile.count') do
      post :create, profile: { comment: @profile.comment, date_of_birthday: @profile.date_of_birthday, first_name: @profile.first_name, last_name: @profile.last_name, photo_url: @profile.photo_url, rank: @profile.rank, third_name: @profile.third_name, worker_id: @profile.worker_id }
    end

    assert_redirected_to profile_path(assigns(:profile))
  end

  test "should show profile" do
    get :show, id: @profile
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @profile
    assert_response :success
  end

  test "should update profile" do
    patch :update, id: @profile, profile: { comment: @profile.comment, date_of_birthday: @profile.date_of_birthday, first_name: @profile.first_name, last_name: @profile.last_name, photo_url: @profile.photo_url, rank: @profile.rank, third_name: @profile.third_name, worker_id: @profile.worker_id }
    assert_redirected_to profile_path(assigns(:profile))
  end

  test "should destroy profile" do
    assert_difference('Profile.count', -1) do
      delete :destroy, id: @profile
    end

    assert_redirected_to profiles_path
  end
end
